﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Service_01.Models
{

    public class HealthSummary
    {
        public string healthState { get; set; }
    }

    public class TargetsModel
    {
        public string displayName { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public bool isMandatory { get; set; }
        public bool isSecret { get; set; }
        public bool isMultiline { get; set; }
        public bool isTargetDisplayName { get; set; }
        public string valueType { get; set; }
        public string description { get; set; }
        public string verificationRegex { get; set; }
        public string defaultValue { get; set; }
    }

    public class Root
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string category { get; set; }
        public List<TargetsModel> inputFields { get; set; }
        public DateTime lastValidated { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public bool @readonly { get; set; }
        public DateTime lastEditTime { get; set; }
        public string lastEditUser { get; set; }
        public HealthSummary healthSummary { get; set; }
    }
}
