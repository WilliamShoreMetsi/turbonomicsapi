﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using Newtonsoft.Json;
using System.Net;
using API_Service_01.Models;

namespace API_Service_01.API
{
    public class Authentication
    {
        AuthenticationModel authModel = new AuthenticationModel();
        public string Authenticate()
        {

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            var authToken = "";

            var client = new RestClient("https://10.22.66.3/api/v3/login");

            var request = new RestRequest("https://10.22.66.3/api/v3/login", Method.Post);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("username", "administrator");
            request.AddParameter("password", "M3tsiT3ch!23");
            var response = client.Execute(request);
            
            foreach(var item in response.Headers)
            {
               if(item.Name == "Set-Cookie")
                {
                    authToken = item.Value.ToString();
                    authToken = authToken.Remove(authToken.IndexOf(";"));
                }
            }

            //var authToken = JsonConvert.DeserializeObject<AuthenticationModel>(response.Content);
            //return authToken.authToken;
            return authToken;
        }
  
    }

}
